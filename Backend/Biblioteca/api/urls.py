
from django.urls import path, include
from .views import (AlumnoView, EditorialView, BuscarLibroView, LibroView, AutorView, PrestamoView, EjemplarView, AlumnoRegistroView, ejemplares_masivos, HistorialView, DevolucionView, current_user_info, log_out_user, ChangePassword,FavoritoView, PrestamoUserView, SuspensionView, AmonestacionView, )
from rest_framework.routers import DefaultRouter

from rest_framework_simplejwt import views as jwt_views

router = DefaultRouter()
router.register('alumnos', AlumnoView, basename='alumnos')
router.register('autores', AutorView, basename='autores')
router.register('editoriales', EditorialView, basename='editoriales')
router.register('buscar-libros', BuscarLibroView, basename='buscar-libros')
router.register('prestamos', PrestamoView, basename='prestamos')
#router.register('historial', HistorialView, basename='historial')
router.register('favoritos', FavoritoView, basename='favoritos')
router.register('libros', LibroView, basename='libros')
router.register('amonestaciones', AmonestacionView, basename='amonestaciones')
router.register('suspensiones', SuspensionView, basename='suspensiones')


urlpatterns = [
    path('', include(router.urls)),
    path('registro-alumno/', AlumnoRegistroView.as_view()),
    path('ejemplares/', EjemplarView.as_view()),
    path('ejemplares/<int:pk>/', EjemplarView.as_view()),
    path('login/', jwt_views.TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('refresh-token/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
    path('registrar-ejemplares/', ejemplares_masivos),
    path('devolucion/', DevolucionView.as_view()),
    path('current-user-info/', current_user_info),
    path('logout/', log_out_user),
    path('change-password/', ChangePassword.as_view()),
    path('historial/', HistorialView.as_view()),
    path('mis-prestamos/', PrestamoUserView.as_view()),
]