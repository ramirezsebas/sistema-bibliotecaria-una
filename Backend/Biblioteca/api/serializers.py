from rest_framework import serializers
from django.contrib.auth.models import User
from .models import Alumno, Editorial, Libro, Autor, Prestamo, Ejemplar, Favorito, Amonestacion, Suspension

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model=User
        fields=('id', 'username',)

class UserAlumnoSerializer(serializers.ModelSerializer):
    class Meta:
        model=User
        fields=('username', 'first_name', 'last_name', 'email',)

class LogOutSerializer(serializers.Serializer):
    token = serializers.CharField()

class ChangePasswordSerializer(serializers.Serializer):
    old_password = serializers.CharField(max_length=50)
    new_password = serializers.CharField(max_length=50)
    confirm_password = serializers.CharField(max_length=50)

class AlumnoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Alumno
        fields = '__all__'

class AlumnoRegistroSerializer(serializers.Serializer):
    nro_ci = serializers.IntegerField()
    nombre = serializers.CharField(max_length=100)
    apellido = serializers.CharField(max_length=100)
    carrera = serializers.CharField(max_length=50)
    numero_telef = serializers.CharField(max_length=20)
    email_dir = serializers.CharField(max_length=50)

class AutorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Autor
        fields = '__all__'

class EditorialSerializer(serializers.ModelSerializer):
    class Meta:
        model = Editorial
        fields = '__all__'

class LibroSerializer(serializers.ModelSerializer):
    class Meta:
        model = Libro
        fields = '__all__'

class LibroCustomSerializer(serializers.Serializer):
    cod = serializers.IntegerField()
    titulo = serializers.CharField(max_length=100)
    autor = serializers.CharField(max_length=100)
    editorial = serializers.CharField(max_length=100)
    anio = serializers.IntegerField()
    temas = serializers.CharField(max_length=100)  

class EjemplarSerializer(serializers.ModelSerializer):
    libro = serializers.CharField(max_length=100)
    class Meta:
        model = Ejemplar
        fields = '__all__'

class EjemplarSerializerRegistro(serializers.ModelSerializer):
    class Meta:
        model = Ejemplar
        fields = '__all__'

class EjemplaresMasivosSerializer(serializers.Serializer):
    cod_libro = serializers.IntegerField()
    num_ejemplares = serializers.IntegerField()

class PrestamoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Prestamo
        fields = '__all__'

class DevolucionSerializer(serializers.Serializer):
    ci_alumno = serializers.IntegerField()
    id_ejemplar = serializers.IntegerField()

class FavoritoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Favorito
        fields = ('libro',)

class AmonestacionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Amonestacion
        fields = ('alumno', 'descripcion',)

class SuspensionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Suspension
        fields = ('alumno', 'fecha_fin', 'descripcion',)

