# Generated by Django 3.0.6 on 2020-05-21 21:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0005_prestamo_fecha_limite'),
    ]

    operations = [
        migrations.AlterField(
            model_name='prestamo',
            name='fecha_limite',
            field=models.DateField(),
        ),
    ]
