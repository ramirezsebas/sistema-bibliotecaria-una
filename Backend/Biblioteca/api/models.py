from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Autor(models.Model):
    cod = models.IntegerField(primary_key=True)
    nombre = models.CharField(max_length=100)

    def __str__(self):
        return self.nombre

class Editorial(models.Model):
    cod = models.IntegerField(primary_key=True)
    nombre = models.CharField(max_length=100)

    def __str__(self):
        return self.nombre

class Libro(models.Model):
    cod = models.IntegerField(primary_key=True)
    titulo = models.CharField(max_length=100)
    autor = models.ForeignKey(Autor, on_delete=models.CASCADE)
    editorial = models.ForeignKey(Editorial, on_delete=models.CASCADE)
    anio = models.IntegerField()
    temas = models.CharField(max_length=100)

    def __str__(self):
        return self.titulo

class Ejemplar(models.Model):
    libro = models.ForeignKey(Libro, on_delete=models.CASCADE)
    disponible = models.BooleanField(default=True)

    def __str__(self):
        libro = Libro.objects.get(pk=self.libro.cod)
        return libro.titulo + " - ej:{}".format(self.id)

class Alumno(models.Model):
    nro_ci = models.IntegerField(primary_key=True)
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    nro_telef = models.CharField(max_length=20)
    carrera = models.CharField(max_length=50)

    def __str__(self):
        return self.user.get_full_name()

class Prestamo(models.Model):
    ejemplar = models.ForeignKey(Ejemplar, on_delete=models.CASCADE)
    alumno = models.ForeignKey(Alumno, on_delete=models.CASCADE)
    fecha_prestamo = models.DateField()
    fecha_devolucion = models.DateField()
    fecha_entrega = models.DateField(default=None, null=True)

class Amonestacion(models.Model):
    alumno = models.ForeignKey(Alumno, on_delete=models.CASCADE)
    descripcion = models.CharField(max_length=140)

class Suspension(models.Model):
    alumno = models.ForeignKey(Alumno, on_delete=models.CASCADE)
    fecha_inicio = models.DateField()
    fecha_fin = models.DateField()
    descripcion = models.CharField(max_length=140)

class Favorito(models.Model):
    alumno = models.ForeignKey(Alumno, on_delete=models.CASCADE)
    libro = models.ForeignKey(Libro, on_delete=models.CASCADE)
