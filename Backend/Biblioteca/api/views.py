from django.http import HttpResponseRedirect
from django.contrib.auth.models import User
from rest_framework import permissions, status, viewsets, mixins
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework.views import APIView
from .serializers import (UserSerializer, AlumnoSerializer, EditorialSerializer, LibroSerializer, AutorSerializer, PrestamoSerializer, EjemplarSerializer , AlumnoRegistroSerializer,
EjemplarSerializerRegistro, LibroCustomSerializer, EjemplaresMasivosSerializer, DevolucionSerializer, UserAlumnoSerializer, LogOutSerializer, ChangePasswordSerializer, FavoritoSerializer, AmonestacionSerializer, SuspensionSerializer, )
from .models import (Alumno, Editorial, Libro, Autor, Prestamo, Ejemplar, Suspension, Favorito, Suspension, Amonestacion, )

from rest_framework_simplejwt.authentication import JWTAuthentication

from django.db.models import Q
from random import SystemRandom
from django.core.mail import send_mail
from django.conf import settings

from rest_framework_simplejwt.tokens import RefreshToken

from datetime import date, datetime, timedelta

from .permissions import IsAdminUserOrReadOnly

@api_view(['GET'])
def current_user_info(request):
    """
        Responde únicamente a un GET request devolviendo los datos del
        alumno una vez que el mismo haya iniciado sesión
    """
    if request.user.id == None:
        return Response({"error":"Debe iniciar sesión."})

    # Obtenemos el usuario que hizo la petición
    user = request.user
    user_serializer = UserAlumnoSerializer(user)

    # Obtenemos el registro del alumno correspondiente al usuario
    try:
        alumno = Alumno.objects.get(user_id=user.id)
        alumno_serializer = AlumnoSerializer(alumno)
    except Alumno.DoesNotExist:
        return Response(dict(user_serializer.data))

    user_dict = dict(user_serializer.data)
    alumno_dict = dict(alumno_serializer.data)

    # Colocamos los datos obtenidos de los modelos User y Alumno en un solo diccionario
    response_dict = user_dict.copy()
    response_dict.update(alumno_dict)

    return Response(response_dict)

@api_view(['POST'])
def log_out_user(request):
    """
        Administra POST requests que tengan como cuerpo un campo 'refresh'
        conteniendo el refresh token asignado al usuario al iniciar sesión,
        luego se encarga de introducir este token en una blacklist para
        invalidarlo
    """
    serializer = LogOutSerializer(data=request.data)

    if serializer.is_valid():
        token_str = serializer.validated_data.get('token')
        token = RefreshToken(token_str)
        token.blacklist()
        msg = {"success":"La sesión ha sido cerrada."}
        return Response(msg)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
@permission_classes((permissions.IsAdminUser,))
def ejemplares_masivos(request):
    """
        Esta vista permite registrar un número predeterminado de ejemplares
        correspondientes a cierto libro, responde únicamente a POST requests
        que tengan como cuerpo los campos 'cod_libro' y 'num_ejemplares'
    """
    serializer = EjemplaresMasivosSerializer(data=request.data)

    if serializer.is_valid():
        # Obtenen los datos contenidos en el serializer
        cod_libro = serializer.validated_data.get('cod_libro')
        num_ejemplares = serializer.validated_data.get('num_ejemplares')

        # Crea la cantidad de ejemplares especificados en la petición
        for x in range(0, num_ejemplares):
            Ejemplar.objects.create(libro_id = cod_libro, disponible=True)
            
        # Obtiene todos los ejemplares registrados del libro especificado
        ejemplares = Ejemplar.objects.filter(libro_id=cod_libro)
        serializer_ej = EjemplarSerializer(ejemplares, many=True)

        return Response(serializer_ej.data, status=status.HTTP_201_CREATED)
    return Response(serializer_ej.errors, status=status.HTTP_400_BAD_REQUEST)

class AlumnoRegistroView(APIView):

    """
        Esta vista administra el registro de alumnos, por lo que responde únicamente a 
        POST requests que contengan todos los datos necesarios del alumno para su
        registro. Se encarga de generar automáticamente username y password para
        luego enviarlos al email del alumno
    """

    permission_classes = [permissions.IsAdminUser,]

    def gen_password(self):
        cryptogen = SystemRandom()
        valores = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789<=>@#%&+'
        longitud = 12

        passwd = ''

        while longitud > 0:
            passwd += cryptogen.choice(valores)
            longitud -= 1

        return passwd

    def gen_username(self, nombre, apellido, ci):

        primer_nombre = nombre.strip().split(' ')[0]
        primer_apellido = apellido.strip().split(' ')[0]
        str_ci = str(ci)
        l = len(str_ci)

        username = (primer_nombre + primer_apellido).lower() + str_ci[l-3:l]

        query_user = User.objects.filter(username=username)

        valores = '0123456789'
        cryptogen = SystemRandom()
        longitud = 3
        sufix = ''

        while query_user.exists() == True:
                
            while longitud > 0:
                sufix += cryptogen.choice(valores)
                longitud -= 1

            username = (primer_nombre + primer_apellido).lower() + sufix
            query_user = User.objects.filter(username=username)

        return username

    def send_user_credential(self, email_dir, username, password):
        subject = "Credenciales para iniciar sesión"
        message = """
        Username: {}\n
        Password: {}\n 
        """.format(username, password)
        email_from = settings.EMAIL_HOST_USER
        email_to = list()
        email_to.append(email_dir)

        send_mail(subject, message, email_from, email_to, fail_silently=False)


    def post(self, request, format=None):
        serializer = AlumnoRegistroSerializer(data=request.data)
        if serializer.is_valid():
            
            nombre = serializer.validated_data['nombre']
            apellido = serializer.validated_data['apellido']
            ci = serializer.validated_data['nro_ci']
            email_dir = serializer.validated_data['email_dir']

            username = self.gen_username(nombre, apellido, ci)
            password = self.gen_password()
            
            usuario = User(first_name=nombre, last_name=apellido, username=username, email=email_dir)
            usuario.set_password(password)
            usuario.save()

            self.send_user_credential(email_dir, username, password)

            telef = serializer.validated_data['numero_telef']
            carrera = serializer.validated_data['carrera']
            alumno = Alumno(nro_ci=ci, user=usuario, nro_telef=telef, carrera=carrera)
            alumno.save()
            
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class EjemplarView(APIView):

    """
        Esta vista puede responder tanto a POST requests como a GET requests, por lo que
        se puede consultar la información de una ejemplar en particular, de un grupo o
        de todos los ejemplares almacenados en la base de datos de acuerdo a parámetros
        como id del libro al cual pertenecen o disponibilidad.
        También se puede registrar ejemplares de forma unitaria especificando en el body 
        del POST request el id del libro.
    """

    permission_classes = [IsAdminUserOrReadOnly,]

    def get_objects(self):
        # Se obtiene el valor del parámetro de petición
        libro_id = self.request.query_params.get('libro-id')
        disponible = self.request.query_params.get('disponible')

        # Se verifica si se ha fijado un valor al parámetro de petición
        if libro_id == None:
            # Si no se ha fijado el parámetro, se listan los ejemplares de acuerdo a su disponibilidad
            if disponible is not None:
                return Ejemplar.objects.filter(disponible=disponible)
            # Si tampoco se especifica la disponibilidad, listar todos los ejemplares
            return Ejemplar.objects.all()
        
        # Si se especificaron el id y la disponibilidad, se filtran los ejemplares de acuerdo a dichos datos
        if disponible is not None:
            query = Ejemplar.objects.filter(Q(libro=libro_id) & Q(disponible=disponible))
        else:
            query = Ejemplar.objects.filter(libro=libro_id)

        return query

    def get(self, request, pk=None):
        if pk == None:
            ejemplares=self.get_objects()
            serializer=EjemplarSerializer(ejemplares, many=True)
        else:
            ejemplar=Ejemplar.objects.get(pk=pk)
            serializer=EjemplarSerializer(ejemplar)
 
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = EjemplarSerializerRegistro(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        ejemplar = Ejemplar.objects.get(pk=pk)
        ejemplar.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class DevolucionView(APIView):

    """
        Por medio de esta vista se puede registrar una devolución utilizando un POST
        request con los datos ci_alumno e id_ejemplar. El procedimiento consiste en 
        modificar un registro de la tabla de prestamos.
    """

    permission_classes = [permissions.IsAdminUser,]
    
    def post(self, request):
        serializer = DevolucionSerializer(data=request.data)
        if serializer.is_valid():
            # Obtenemos los datos del serializer
            ci_alumno = serializer.validated_data.get('ci_alumno')
            id_ejemplar = serializer.validated_data.get('id_ejemplar')

            # Obtenemos el registro correspondiente de la tabla de prestamos
            prestamo = Prestamo.objects.filter(Q(alumno_id=ci_alumno) & Q(ejemplar_id=id_ejemplar))[0]
            # Obtenemos la fecha del dia de la devolucion
            today = date.today()
            # Modificamos el campo correspondiente a la fecha de entrega
            prestamo.fecha_entrega = today

            # Verificamos si la devolución es tardía para amonestar al alumno
            if prestamo.fecha_entrega > prestamo.fecha_devolucion:
                descripcion = "Devolución tardía de libro."
                Amonestacion.objects.create(alumno_id=ci_alumno, descripcion=descripcion)

            # Verificamos si tiene 3 amonestaciones para suspender al alumno
            amonestaciones = Amonestacion.objects.filter(alumno_id=ci_alumno)
            if len(amonestaciones) >= 3:
                inicio = date.today()
                fin = inicio + timedelta(days=28)
                descripcion = "Acumulación de tres amonestaciones"
                # Registramos una suspensión
                Suspension.objects.create(alumno_id=ci_alumno, fecha_inicio=inicio, fecha_fin=fin, descripcion=descripcion)
                # Eliminamos todas las amonestaciones acumuladas
                for amonestacion in amonestaciones:
                    amonestacion.delete()

            # Obtenemos el registro del ejemplar devuelto
            ejemplar = Ejemplar.objects.get(id=id_ejemplar)
            # Y especificamos que a partir de este momento se encuentra nuevamente disponible
            ejemplar.disponible = True

            # Se guardan los cambios hechos
            ejemplar.save()
            prestamo.save()

            msg = {"success":"La devolución ha sido registrada con éxito."}

            return Response(msg, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class ChangePassword(APIView):

    """
        Esta vista permite al usuario cambiar su contraseña mediante un POST request que 
        en su cuerpo contenga la contraseña actual y la nueva contraseña con una confirmación
    """
    
    def post(self, request):

        serializer = ChangePasswordSerializer(data=request.data)

        if serializer.is_valid():
            # Obtiene los datos del usuario que efectúa la petición
            user_serializer = UserSerializer(request.user)
            user_id = user_serializer.data.get('id')

            # Obtiene el registro del usuario que efectúa la petición
            user = User.objects.get(id=user_id)

            # Obtiene los datos enviados en el cuerpo de la petición
            old_password = serializer.data.get('old_password')
            new_password = serializer.data.get('new_password')
            confirm_password = serializer.data.get('confirm_password')

            # Se obtiene el resultado de la comparación entre la nueva contraseña y su confirmación
            match_password = new_password == confirm_password 

            # Verifica que la contraseña actual sea correcta y que la nueva coincida con su confirmación
            if user.check_password(old_password) & match_password:
                msg = {"success":"Contraseña cambiada con éxito."}
                user.set_password(new_password)
            else:
                msg = {"error":"La contraseña actual es incorrecta o la nueva contraseña no coincide con su confirmación."}

            return Response(msg)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class AlumnoView(viewsets.GenericViewSet, mixins.ListModelMixin, mixins.RetrieveModelMixin, mixins.UpdateModelMixin):
    """
        Esta vista permite peticiones GET para obtener el registro de un alumno, peticiones
        POST para guardar un nuevo registro en la base de datos y peticiones PUT para actualizar
        los campos de un registro
    """
    queryset = Alumno.objects.all()
    serializer_class = AlumnoSerializer
    permission_classes = [permissions.IsAdminUser,] 

class AutorView(viewsets.ModelViewSet):
    """
        Esta vista permite peticiones GET para obtener el registro de un autor, peticiones
        POST para guardar un nuevo registro en la base de datos y peticiones PUT para actualizar
        los campos de un registro
    """
    queryset = Autor.objects.all()
    serializer_class = AutorSerializer
    permission_classes = [IsAdminUserOrReadOnly,]

class EditorialView(viewsets.ModelViewSet):
    """
        Esta vista permite peticiones GET para obtener el registro de una editorial, peticiones
        POST para guardar un nuevo registro en la base de datos y peticiones PUT para actualizar
        los campos de un registro
    """
    queryset = Editorial.objects.all()
    serializer_class = EditorialSerializer
    permission_classes = [IsAdminUserOrReadOnly,]

class HistorialView(APIView):
    def get(self, request):
        alumno_ci = Alumno.objects.get(user_id=request.user.id).nro_ci
        prestamos = Prestamo.objects.filter(alumno_id=alumno_ci).exclude(fecha_entrega=None)
        serializer = PrestamoSerializer(prestamos, many=True)
        return Response(serializer.data)

class LibroView(viewsets.ModelViewSet):
    serializer_class = LibroSerializer
    permission_classes = [IsAdminUserOrReadOnly,]
    queryset = Libro.objects.all()

class BuscarLibroView(viewsets.GenericViewSet, mixins.ListModelMixin):
    serializer_class = LibroCustomSerializer
    permission_classes = [permissions.AllowAny,]

    def get_queryset(self):
        # Obtenemos la búsqueda introducida por el cliente
        busqueda = self.request.query_params.get('busqueda')

        # Si no se buscó nada simplemente se devuelven todos los registros de la tabla
        if busqueda == None:
            return Libro.objects.all()

        # Creamos una lista de palabras que forman parte de la búsqueda
        busqueda_palabras = busqueda.strip().split(' ')

        # Creamos una expresión regular para filtrar los datos
        regexp = ''
        for palabra in busqueda_palabras:
            regexp += palabra + '|'
        regexp = regexp[0:len(regexp)-1]

        # Obtenemos todos los libros que coinciden con la búsqueda

        query = Libro.objects.all()

        def query_decorator(filter):
            def inner(*args):
                anterior = args[0]
                query = filter(*args)
                if query.exists() == False:
                    query = anterior
                return query
            return inner

        # Creamos funciones para cada filtro que necesitemos hacer y aplicamos a c/u el decorador

        @query_decorator
        def filtrar_titulo(query, regexp):
            return query.filter(titulo__iregex=regexp)

        @query_decorator
        def filtrar_temas(query, regexp):
            return query.filter(temas__iregex=regexp)

        @query_decorator
        def filtrar_autor(query, regexp):
            # Obtenemos una lista de los id de todos los autores que coincidieron con la búsqueda
            authors = Autor.objects.filter(nombre__iregex=regexp)
            authors_id = list()
            for author in authors:
                authors_id.append(author.cod)
            return query.filter(autor_id__in=authors_id)

        @query_decorator
        def filtrar_editorial(query, regexp):
            # Obtenemos una lista de los id de todas las editoriales que coincidieron con la búsqueda
            editoriales = Editorial.objects.filter(nombre__iregex=regexp)
            editoriales_id = list()
            for editorial in editoriales:
                editoriales_id.append(editorial.cod)
            return query.filter(editorial_id__in=editoriales_id)

        query = filtrar_autor(query, regexp)
        query = filtrar_temas(query, regexp)
        query = filtrar_titulo(query, regexp)
        query = filtrar_editorial(query, regexp)

        return query
    
class PrestamoView(viewsets.GenericViewSet, mixins.ListModelMixin, mixins.RetrieveModelMixin, mixins.UpdateModelMixin, mixins.CreateModelMixin):
    
    serializer_class = PrestamoSerializer
    permission_classes = [permissions.IsAdminUser,]

    def get_queryset(self):
        alumno_ci = self.request.query_params.get('ci')

        if alumno_ci == None:
            return Prestamo.objects.all()

        query = Prestamo.objects.filter(Q(alumno_id=alumno_ci) & Q(fecha_entrega=None))

        return query

    def create(self, request):
        serializer = PrestamoSerializer(data=request.data)
        if serializer.is_valid():
            # Verificar si el alumno está suspendido
            ci = serializer.validated_data.get('alumno')
            today = date.today()

            suspensiones = Suspension.objects.filter(Q(alumno_id=ci) & Q(fecha_fin__gte=today))

            if suspensiones.exists():
                error = {"error":"El alumno está suspendido."}
                return Response(error)
            
            # Verificar si el libro está disponible

            ejemplar = serializer.validated_data.get('ejemplar')
            disponible = ejemplar.disponible

            if disponible == False:
                error = {"error":"El Libro requerido no está disponible."}
                return Response(error)

            serializer.save()
            ejemplar.disponible = False
            ejemplar.save()

            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class PrestamoUserView(APIView):
    permission_classes = [permissions.IsAuthenticated, ]
    def get(self, request):

        user_id = request.user.id
        
        ci_alumno = Alumno.objects.get(user_id=user_id).nro_ci

        prestamos = Prestamo.objects.filter(alumno_id=ci_alumno)

        serializer = PrestamoSerializer(prestamos, many=True)

        return Response(serializer.data)    

class FavoritoView(viewsets.GenericViewSet, mixins.ListModelMixin, mixins.DestroyModelMixin, mixins.CreateModelMixin):
    serializer_class = FavoritoSerializer
    permission_classes = [permissions.IsAuthenticated,]

    def list(self, request):
        user_id = request.user.id
        ci_alumno = Alumno.objects.get(user_id=user_id).nro_ci

        favoritos = Favorito.objects.filter(alumno_id=ci_alumno)

        id_favoritos = list()
        
        for fav in favoritos:
            id_favoritos.append(fav.libro_id)

        libros_favoritos = Libro.objects.filter(cod__in=id_favoritos)
        serializer = LibroCustomSerializer(libros_favoritos, many=True)

        return Response(serializer.data)

    def create(self, request):
        
        serializer = self.serializer_class(data=request.data)

        if serializer.is_valid():
            user = request.user
            alumno = Alumno.objects.get(user_id=user.id)

            libro = serializer.validated_data.get('libro')

            Favorito.objects.create(alumno_id=alumno.nro_ci, libro_id=libro.cod)
        
            return Response({"success":"Libro añadido a favoritos."}, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class SuspensionView(viewsets.ModelViewSet):
    serializer_class = SuspensionSerializer
    permission_classes = [permissions.IsAdminUser,]
    
    def get_queryset(self):
        # Obtenemos el nro de ci del alumno cuyas suspensiones deseamos listar
        ci = self.request.query_params.get('ci')

        # Si no se especifica un nro de ci, se listan todas las suspensiones
        if ci == None:
            return Suspension.objects.all()

        return Suspension.objects.filter(alumno_id=ci)

class AmonestacionView(viewsets.ModelViewSet):
    serializer_class = AmonestacionSerializer
    permission_classes = [permissions.IsAdminUser,]
    
    def get_queryset(self):
        # Obtenemos el nro de ci del alumno cuyas amonestaciones deseamos listar
        ci = self.request.query_params.get('ci')

        # Si no se especifica un nro de ci, se listan todas las amonestaciones
        if ci == None:
            return Amonestacion.objects.all()

        return Amonestacion.objects.filter(alumno_id=ci)

